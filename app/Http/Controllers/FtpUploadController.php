<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class FtpUploadController extends Controller
{
    //
    public function importFilefromFTP()
    {
        try{
            $contents = Storage::disk('sftp')->get('/BARCODE/MOBILE/GR/GOODS_RECEIPT_20230504.txt');
            $conn = Storage::disk('public')->put('FtpFileContent.txt', $contents);
            if($conn === false){
                return false;
            }
            print_r($conn);

        } catch(\Exception $e){
            return 'fail '.$e->getMessage();
        }
    }
}
